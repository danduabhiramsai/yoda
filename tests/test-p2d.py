#! /usr/bin/env python

import yoda, random

p = yoda.Profile2D(5,0.,10., 5,0.,10., "/bar")
for _ in range(10000):
    p.fill(random.gauss(1, 3), random.gauss(1, 2), random.gauss(10, 0.5))
    p.fill(random.gauss(1, 0.3), random.gauss(1, 0.2), random.gauss(1, 0.5))
print(p)

yoda.write([p], "p2d.yoda")
aos = yoda.read("p2d.yoda")
for _, ao in aos.items():
    print(ao)

yoda.write([p], "p2d.dat")
# aos = yoda.read("p2d.dat")
# for _, ao in aos.iteritems():
#     print ao

## Check scatter conversion
s = p.mkScatter()

if p.numBins() != s.numPoints():
    print("FAIL mkScatter() #bin={} -> #point={}".format(p.numBins(), s.numPoints()))
    exit(11)
idx = p.indexAt(1,1)
zVal = p.bin(idx).zMean()
if zVal != s.point(0).z():
    print("FAIL mkScatter() bin0 value={} -> point0 value={}".format(zVal, s.point(0).z()))
    exit(12)
zErr = p.bin(idx).zStdErr()
if zErr != s.point(0).zErrAvg():
    print("FAIL mkScatter() bin0 err={} -> point0 err={}".format(zErr, s.point(0).zErrAvg()))
    exit(13)

