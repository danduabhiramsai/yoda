#include "YODA/Histo.h"
#include "YODA/Scatter.h"
#include "YODA/Utils/MathUtils.h"

#include <cmath>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
using namespace std;
using namespace YODA;

int main() {
  ios_base::sync_with_stdio(0);
  Histo2D h(200, 0, 100, 200, 0, 100);
  const size_t nBins = h.numBins();

  struct timeval startTime;
  struct timeval endTime;
  gettimeofday(&startTime, NULL);
  cout << "Testing fill operation:                  ";
  for (int i=0; i < 2000; i++) {
      size_t idx = h.indexAt(16.0123, 12.213);
      int out = h.fill(16.0123, 12.213, 2);
      if (out != int(idx)) {
          cout << "FAIL" << endl;
          return -1;
      }
  }
  gettimeofday(&endTime, NULL);

  double tS = (startTime.tv_sec*1000000 + startTime.tv_usec)/(double)1000000;
  double tE = (endTime.tv_sec*1000000 + endTime.tv_usec)/(double)1000000;
  if ((tE - tS) > 50.0) {
      cout << "Performance is not sufficient. Probably broken caches?" << endl;
      return -1;
  }
  cout << "PASS (" << tE - tS << "s)" << endl;

  // Testing if fill() function does what it should
  cout << "Does fill() do what it should?           ";
  h.maskBin(h.indexAt(12.3,32.1));
  if (h.numBins() != (nBins-1)) {
    cout << "FAIL" << endl;
    return -1;
  }
  cout << "PASS" << endl;

  return EXIT_SUCCESS;
}
