#! /usr/bin/env python

from __future__ import print_function
import yoda, random

h = yoda.Histo2D(5,0.,10., 5,0.,10., "/foo")
for _ in range(100):
    h.fill(random.gauss(5, 3), random.gauss(5, 2))
    h.fill(1., 1.)
print(h)
print(h.bins())
print(h.bin(2))

yoda.write([h], "h2d.yoda")
aos = yoda.read("h2d.yoda")
for _, ao in aos.items():
    print(ao)

yoda.write([h], "h2d.dat")
# aos = yoda.read("h2d.dat")
# for _, ao in aos.iteritems():
#     print ao

# Check scatter conversion
s = h.mkScatter()

if h.numBins() != s.numPoints():
    print("FAIL mkScatter() #bin={} -> #point={}".format(h.numBins(), s.numPoints()))
    exit(11)
idx = h.indexAt(1,1)
zVal = h.bin(idx).sumW() / h.bin(idx).xWidth() / h.bin(idx).yWidth()
if zVal != s.point(0).z():
    print("FAIL mkScatter() bin0 value={} -> point0 value={}".format(zVal, s.point(0).z()))
    exit(12)
zErr = h.bin(idx).errW() / h.bin(idx).xWidth() / h.bin(idx).yWidth()
if zErr != s.point(0).zErrAvg():
    print("FAIL mkScatter() bin0 err={} -> point0 err={}".format(zErr, s.point(0).zErrAvg()))
    exit(13)

