#! /usr/bin/env python

import yoda, random

h1 = yoda.Histo1D(20, 0.0, 100.0, path="/foo", title="MyTitle")

linspace = yoda.linspace(20, 0.0, 100.0)
h2 = yoda.Histo1D(linspace, path="/bar", title="Linearly spaced histo")

logspace = yoda.logspace(20, 1.0, 64)
h3 = yoda.Histo1D(logspace, path="/baz", title="Log-spaced histo")

NUM_SAMPLES = 1000
for i in range(NUM_SAMPLES):
    exp = - (i-NUM_SAMPLES/2)**2 / float(NUM_SAMPLES/4)
    val = 2.718 ** exp
    h1.fill(val);
    h2.fill(val);
    h3.fill(val);
print(h1.xMean(), "+-", h1.xStdDev())
print(h1)
print(h1.bins())
print(h1.bin(2))
print(h2)
print(h3)


yoda.write([h1,h2,h3], "h1d.yoda")
aos = yoda.read("h1d.yoda")
for _, ao in aos.items():
    print(ao)

yoda.writeFLAT([h1,h2,h3], "h1d.dat")
aos = yoda.read("h1d.dat")
for _, ao in aos.items():
    print(ao)
s = h1.mkScatter()

# Check that the bin scaling is done properly
s1 = h3.mkScatter()
if h3.numBins() != s1.numPoints():
    print("FAIL mkScatter() #bin={} -> #point={}".format(h3.numBins(), s1.numPoints()))
    exit(11)
yVal = h3.bin(1).sumW() / h3.bin(1).xWidth()
if yVal != s1.point(0).y():
    print("FAIL mkScatter() bin0 value={} -> bin0 value={}".format(yVal, s1.point(0).y()))
    exit(12)
yErr = h3.bin(1).errW() / h3.bin(1).xWidth()
if yErr != s1.point(0).yErrAvg():
    print("FAIL mkScatter() bin0 err={} -> point0 err={}".format(yErr, s1.point(0).yErrAvg()))
    exit(13)

