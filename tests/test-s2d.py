#! /usr/bin/env python

import yoda

s = yoda.Scatter2D("/foo")
s.addPoint(3, 0.1, 3, 0.2)
s.addPoint(10, 0.1, 0.2, 5, 0.5, 0.5)

# check setting error sources
s.point(0).setYErrs(0.3)
s.point(0).setYErrs(0.4)
s.point(1).setYErrs(0.5)
s.point(1).setYErrs(1.2)
print(s)

# check setting total uncertainty from sources
assert(abs(s.point(0).yErrs()[0]) - 0.4 < 0.0001)
assert(abs(s.point(1).yErrs()[1]) - 1.2 < 0.0001)

# check if clone correctly copies everythign over
s2 = s.clone() 

# check that write out/read in copies the error breakdown info
yoda.write([s], "s2d.yoda")
aos = yoda.read("s2d.yoda")
for _, ao in aos.items():
    print("ao = " + repr(ao))

    
