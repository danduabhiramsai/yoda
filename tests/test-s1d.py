#! /usr/bin/env python

import yoda

s = yoda.Scatter1D("/foo")
s.addPoint(3, 0.1)
s.addPoint(10, 0.1, 0.2)
print(s)

# check setting error sources
s.point(0).setXErrs(0.3)
s.point(0).setXErrs(0.4)
s.point(1).setXErrs(0.5)
s.point(1).setXErrs(1.2)
print(s)

# check setting total uncertainty from sources
assert(abs(s.point(0).xErrs()[0]) - 0.4 < 0.0001)
assert(abs(s.point(1).xErrs()[1]) - 1.2 < 0.0001)

# check if clone correctly copies everythign over
s2 = s.clone()

# check that write out/read in copies the error breakdown info
yoda.write([s], "s1d.yoda")
aos = yoda.read("s1d.yoda")
for _, ao in aos.items():
    print("ao = " + repr(ao))

