# Scatter (arrays of Points)

YODA's `Scatter` class is a set of [Point objects](Point.md).

```cpp
Scatter2D s;
s.addPoint({1,2}, {{0.1,0.2},{0.3,0.4}});
std::cout << s.numPoints() << std::endl;
for (const auto& p : s.points()) {
  std::cout << p.val(0) << " - " << p.errs()[0].first;
  std::cout << " + " << p.errs()[0].second << "  and  ";
  std::cout << p.val(1) << " - " << p.errs()[1].first;
  std::cout << " + " << p.errs()[1].second << std::endl;
}
```
```
1
1 - 0.1 + 0.2  and  2 - 0.3 + 0.4
```
