# Installation

The easiest way to start using YODA is via the [Docker system](https://www.docker.com/), 
which is like a lightweight Linux virtual machine that's easily installed and run on Mac, 
Linux and Windows machines.

The rest of these instructions are mainly aimed at users who want to natively install 
and run a releas of YODA (and Rivet) on their machine.

## Installation YODA together with Rivet and all dependencies

The simplest Rivet installation uses a "bootstrap" script to install Rivet and all 
its dependencies from release tarballs.


### Prerequisites

Python header files are required. On Ubuntu and other Debian Linux derivatives,
you can use this command to install the necessary files system-wide: `sudo apt-get install python-dev`.
You will also need a C++ compiler capable of building the C++17 dialect: on systems like
CERN lxplus you can get such an environment with:
```sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_99/ROOT/v6.22.06/x86_64-centos7-gcc10-opt/ROOT-env.sh
```

### Installation

1. **Download the bootstrap script** into a temporary working directory, and make it executable:
```sh
  cd /scratch/rivet
  wget https://gitlab.com/hepcedar/rivetbootstrap/raw/3.1.7/rivet-bootstrap
  chmod +x rivet-bootstrap
```
(Replace the version string as appropriate if you want to install other versions of Rivet.)

2. **Check the options.** Look at the header of the script to see all variables which you can set, 
e.g. to skip installation of certain dependencies if they are available in your system:
```sh
  less rivet-bootstrap ## and read...
```


3. **Run the script.** By default it will install the whole suite of Rivet dependencies
and Rivet itself to `$PWD/local`, where `$PWD` is the current directory.
We will refer to this installation root path as `ROOT`: where the word `ROOT`
appears below, in commands for you to type in, you should replace it with the actual
installation prefix you used when running `rivet-bootstrap`

If you need to change that, specify the corresponding values on the command line. i
Other variables used by the script can be set at the same time if you wish. Examples:
```sh
# To install to $PWD/local:
./rivet-bootstrap
```
or
```sh
# To install to ~/software/rivet
INSTALL_PREFIX=$HOME/software/rivet MAKE="make -j8" ./rivet-bootstrap
```

*Other variables used in the script, such as version numbers, can be overridden
 this way. Those other than `INSTALL_PREFIX` and `MAKE` are mainly of interest
 to developers, though. It is possible that you will need a developer-style
 setup of the Rivet development version rather than a released version: in this
 case you will need to have essential developer tools like autotools and Cython
 already installed, then pass `INSTALL_RIVETDEV=1` as a command-line variable.*

## Installation from source

Clone the repo and run
```sh
autoreconf -vi
```
or alternatively download a YODA release tarball

Run the `configure` script
```sh
./configure --prefix=${PWD}/local
```

Check the `--help` flag for additional options.
Build and install
```sh
make -j4 && make -j4 install
```

## Build a program

Once you've sourced the environment script,
you can build a program like so

```sh
g++ mytest.cc -o mytest `yoda-config --cflags --libs` --std=c++17
```
