# Plotting style customisations (work in progress)

[ToC]

## Introduction

Cosmetic customisations for the plotting API can be steered using
a myriad of tag-value pairs that can be passed to the plotting
command.

If the intention is to modify a property of the curve, the tag
should be appended to the file name that contains the relevant
numerical data for the curve.

If the intention is to modify a property of the canvas, the tag
can be supplied with a dummy `PLOT` argument on the command like
e.g. like so
```
rivet-mkhtml file1.yoda.gz:"Title=Curve 1" file2.yoda.gz:"Title=Curve 2" PLOT:LogY=0
```
where quotation marks can be used to escape spaces and TeX strings in bash.

## Config files

For a more nuanced disambiguation as to which plot should respect which tags,
it's best to supply a config file using the `-c` flag:
```
rivet-mkhtml file1.yoda.gz:"Title=Curve 1" file2.yoda.gz:"Title=Curve 2" -c config.plot
```
where the config file could look like this:
```
BEGIN PLOT /histo1
LogY=1
XLabel=$p_\mathrm{T}$ [GeV]
XLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}$ [fb/GeV]
END PLOT

BEGIN PLOT /histo2
LogY=0
XLabel=$mp_\mathrm{inv}$ [GeV]
XLabel=$\mathrm{d}\sigma / \mathrm{d}m_\mathrm{inv}$ [fb/GeV]
END PLOT
```
Note that the tags from each block will only be applied
to a histogram if its path matches the expression after
`BEGIN PLOT` where a wildcard (e.g. `.*`) could be used
to match multiple objects with similar path structure.


## Custom curve identifiers

The curves from any given file will be identified in the
resulting Python scripts by their file name, unless the
user chooses to provide a custom identifier using the
`Name` tag. This is useful to avoid long clumsy identifiers,
e.g. like so
```
a_long_file_name_possibly_01_containing_02_numbers_03.yoda:Name=curve1
```


## Titles and labelling

### Title

When appended to the filename, it will determine the name of the
curve used in the corresponding legend entry:

```
file.yoda:"Title=Model 1"
```

When added to the `PLOT` section in a config file, the string value
will be added at the top of the figure.
```
Title=Differential cross-section as a function of $p_\mathrm{T}$
```

## Axes

### XLabel, YLabel, Zlabel

These tags go into the `PLOT` section and will determine the
corresponding axis labels:

```
XLabel=<label>
YLabel=<label>
ZLabel=<label>
```

The separation of the the axis label with respect to the axis can be changed using
```
XLabelSep=<value>
YLabelSep=<value>
```
where the default values are -0.15 for the x-axis and -0.11 for the y-axis.

### LogX, LogY, LogZ

These tags go into the `PLOT` section and will determine whether
the corresponding axis is on a logarithmic or linear scale:

```
LogX=<0|1>
LogY=<0|1>
LogZ=<0|1>
```

### Axis range clipping

The range of an axis can be clipped by specifing min/max
values along the axis:

```
XMin=<value>
XMax=<value>
YMin=<value>
YMax=<value>
ZMin=<value>
ZMax=<value>
```

### Axis tick marks

Whethere or not to have tick marks
on both sides of the figure can be
steered with these tags:

```
XTwosidedTicks=<0|1>
YTwosidedTicks=<0|1>
```

The major tick marks can be explicitly provided using
```
XMajorTicksMarks=<list>
YMajorTicksMarks=<list>
```

The minor tick marks can be explicitly provided using
```
XMinorTicksMarks=<list>
YMinorTicksMarks=<list>
```
or suppressed entirely using
```
XMinorTicksMarks=0
YMinorTicksMarks=0
```

### Axis tick labels

Custom major tick labels can be provided using
coordinate-string pairs as follows
```
XCustomMajorTicks=0	zero	1	one	2	two
```
where the list items are seperated by a tab.

If the strings get too long, consider
rotating them by providing an angle
```
XMajorTicksAngle=45
```

The tick labels can be removed entirely using
```
PlotTickLabels=<0|1>
```
on both axes or just on a specifc axis
using the more nuanced
```
PlotXTickLabels=<0|1>
PlotYTickLabels=<0|1>
```

### Binning

The x-axis can be rebinned by an integer factor using
```
Rebin=<value>
```
Note that this will attempt to rebin the scatter objects
on the fly, which may not be a well defined operation,
e.g. in the case of bin gaps, masked bins, or if some
of the points are overlapping.
If an issue is encountered the re-binning is skipped.


## Ratio panel(s)

A ratio panel is included by default. This can be (de-)activated using
```
RatioPanel=<0|1>
```

Additional ratio panels can be added by appending one or more digits
to the tag e.g.
```
RatioPanel1=<0|1>
RatioPanel2=<0|1>
RatioPanel3=<0|1>
...
```

Most tags available for the main figure panel
can be set explicitly for the ratio panels by
prepending their name, e.g. like so
```
RatioPanel1LogY=0
RatioPanel2LogY=1
RatioPanel3YLabel='Ratio'
RatioPanel4YLabel='Double ratio'
```

Different ratio modes are available
```
RatioPlotMode=<mcdata|datamc|deviation>
```

When including many curves, it's possible
to select different subsets for different
ratio panels, e.g. like
```
RatioPanel1DrawOnly=histo1 histo2
RatioPanel1Reference=histo1
RatioPanel2DrawOnly=histo2 histo3
RatioPanel2Reference=histo2
```
Custom string could be set for the histogram
identifiers (cf. above).


## Canvas setup

### Sizes and margins

Canvas width and height are automatically
determined from the figure panel width,
the figure panel height (including additional
ratio panels) and the margins. All of these can
be scaled relative to the default size.

The canvas width can be scaled using
```
PlotXSize=<xscale>
```
The height of the main panel can be scaled using
```
PlotYSize=<yscale>
```
Additional ratio panels can be scaled in the same way
by adding their respective prefix e.g.
```
RatioPlotYSize=<yscale>
```
These factors also determine the aspect ratio,
which is tyically 2:1 for a canvas with a
main panel and a single ratio panel.

The canvas margins can be scaled relative to
their default widths like so
```
LeftMargin=<scale>
RightMargin=<scale>
TopMargin=<scale>
BottomMargin=<scale>
```
where the default scale factor is 1.

The font sizes can be changed using the following tags
```
AxisLabelSize=<value>   # fontsize of the axis labels
XTickSize=<value>       # fontsize of the x-tick labels
YTickSize=<value>       # fontsize of the y-tick labels
TitleSize=<value>       # fontsize of the figure title
LegendFontSize=<value>  # fontsize of the legend labels
```
The default font size is `10`.


### Frame color

The background color for the margin around the plot can be set using
```
FrameColor=<color>
```

## Legend(s)

A legend is included by default. This can be (de-)activated using
```
Legend=<0|1>
```

Additional legends can be added by appending one or more digits
to the tag e.g.
```
Legend1=<0|1>
Legend2=<0|1>
Legend3=<0|1>
...
```

The legend coordinates with respect to the main panel can be set using
```
LegendXPos=<xpos>
LegendYPos=<ypos>
```
In order to move the legend outside of the figure, one can use coordindates
outside of the [0,1] range. Note that by default an attempt is made to try
and identify an optimal position for the legend with respect to the reference
curve.

The x- and y-coordinates can be anchored on the legend using
```
LegendAnchor=<upper left|upper center|upper right|center left|center|center right|lower left|lower center|lower right>
```

The legend alignment can be toggled using
```
LegendAlign=<l|r>
```
where the default position of the marker symbols with respect to the curve labels
will depend on the default positioning of the legend with respect to the reference
curve.

In order to suppress curves from the legend, one can specify the relevant
subset of curve identifiers e.g. like so
```
LegendOnly=histo1 histo3
```

An optional title for the legend can be set using
```
LegendTitle=<label>
```
The LaTeX `\newline` tag can be used to break up longer legend titles or entries.


## Curve styles (append to file!)

To toggle between histogram and reference-data-like scatter styles use
```
ConnectBins=<0|1>
```

If the curve is plotted as a histogram, you can choose to a fill color and opacity
for the area below the curve like so
```
FillColor=<color>
FillOpacity=<value>
```
where the default opacity is 1.0 (i.e. no transparency).

The error bars can be (de-)activated using
```
ErrorBars=<0|1>
```
By default this toggles a consistent behaviour across all panels.
It's also possible to achieve a different behaviour in the ratio panels
by also specifying
```
RatioPlotErrorBars=<0|1>
```
in which case `ErrorBar` only toggles the main panel.

In order to draw little perpendicular ticks at the end caps of the error bars,
one can set a non-zero error-cap size, like so
```
ErrorCapSize=<value>
```
where the default value is 0. A suitable non-zero value could be `ErrorCapSize=2`.

The y-values can be scaled on the fly using
```
Scale=<scale>
```
The size of the error bars can be influenced using the `ErrorPattern` tag.
If the input file(s) contain(s) estimate-like objects that have an error
breakdown, this regex-like pattern will be propagated such that only
error sources matching the patter will be included in the
sum-in-quadrature-based total uncertainty. For instance, a suitable
regex to select uncorrelated sources of uncertainty only could be
`ErrorPattern=^stat|^uncor`.

The curve width can be scaled using
```
LineWidth=<scale>
```
The default value is 1.

The curve style can be changed using
```
LineStyle=<solid|dotted|dashed|dashdot>
```

The marker style can be set using
```
MarkerStyle=<.|,|o|v|^|...>
```
A list of marker styles is available [here](https://matplotlib.org/stable/api/markers_api.html).

The size of the markers can be set using
```
MarkerSize=<size>
```
Default is 2.

The color can be set using
```
LineColor=<color>
```

A connecting line between markers can be drawn using
```
ConnectMarkers=<0|1>
```

## Band styles (append to file!)

If a `BandUncertainty` is provided, the corresponding error bars
are plotted as a band. By default this is done consistently
across all panels, but a more fine-grained behaviour can be
achieved by specifying e.g.
```
ErrorBand=<0|1>
RatioPlotErrorBand=<0|1>
```
The values do not need to be Boolean, but could be a list of floating point values
corresponding to the scaled levels of standard deviation for which bands are meant
to be plotted e.g.
```
ErrorBand=1 2
```
would yield the 1- and 2-sigma bands. The values need not be integer.

The error-band color can then be set using
```
ErrorBandColor=<colo1r> <color2> ...
```

The fill style of the band can be set using
```
ErrorBandStyle=<style1> <style2> ...
```
Possible values are `'/', '\', '|', '-', '+', 'x', 'o', 'O', '.', '*'`.

The error band opacity can be set using
```
ErrorBandOpacity=<value1> <value2> ...
```
The value is the percentage opacity, the default value being 0.2.

## Colors

A list of color names in `matplotlib` is
available [here](https://matplotlib.org/stable/gallery/color/named_colors.html).

For 2D plots, the color palette can be set using e.g.
```
ColorMap=cividis
```
A list of color palette names in `matplotlib` is
available [here](https://matplotlib.org/stable/users/explain/colors/colormaps.htm).


## TEXT blocks

### Custom text box

Custom text boxes can be added using `TEXT` blocks:
```
BEGIN TEXT /histo.* Box1
PlotText=\\textbf{\\textit{YODA}}
XPos=0.05
YPos=0.87
FontSize=14
END TEXT

BEGIN TEXT /histo.* Box2
PlotText=$\\sqrt{s}$ = 13 TeV
XPos=0.05
YPos=0.78
END TEXT
```

Here two boxes will be plotted on all figures corresponding
to object paths that end in `histo.*`. The two boxes are
given arbitrary labels (`Box1`, `Box2`).

In order to rotate the text box, pass the following tag:
```
Rotation=<degrees>
```

