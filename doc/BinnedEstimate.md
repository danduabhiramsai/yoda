# BinnedEstimate (inert datasets)

Unlike the [BinnedDbn](BinnedDbn.md) class which is a live
object used for histograms and profiles that updates its
internal state upon a fill, YODA also provides a dedicated
container type for inert quantities which histogram-like
objects turn into once they are "finished",
i.e. no longer fillable: the `BinnedEstimate` class.
This object lends itself nicely to differential
measurements. An example for a `BinnedEstimate` object
in 1D could look like this

```cpp
BinnedEstimate<double> bEst_1D;
BinnedEstimate<int,std::string> bEst_2D;
```

Most frequently, people would deal with
datasets that only have continuous axes.
In that case convenient aliases are defined:

```cpp
Estimate1D e_1D; // Dataset with 1 continuous axis
Estimate2D e_2D; // Dataset with 2 continuous axes
Estimate3D e_3D; // Dataset with 3 continuous axes
EstimateND<m> e_ND; // Dataset with m continuous axes
```

## Constructors

You can construct a `BinnedEstimate` class either
via [Binning object](Binning.md),
via [Axis objects](BinnedAxis.md),
via lists of edes
(e.g. brace-initialised lists or vectors),
or - in the case of all-continuous axes -
you can also specify the number of bins within
a continuous range, leaving it to the class
to work out the edges itself.

```cpp
Estimate1D e1(10, 0, 100); // 10 bins between 0 and 100
std::vector<double> edges = {0, 10, 20, 30, 40, 50};
Estimate1D e2(edges);
BinnedEstimate2D<int, string> e3({ 1, 2, 3 }, { "A", "B", "C" });
```

## Global vs local bin index

See the discussion [here](BinnedDbn.md#global-vs-local-bin-index).

## Masked bins

See the discussion [here](BinnedDbn.md#masking-bins).

## Rebinning

See the discussion [here](BinnedDbn.md#rebinning).

## Bin edges and bin widths

See the discussion [here](BinnedDbn.md#bin-edges-and-bin-widths).

## Covariance matrix

The `covarianceMatrix()` method has optional arguments
to include the overflows (default `false`) or to skip
the off-diagonal elements (default `false`, i.e. include them).

```cpp
Estimate1D e1(5, 0, 1);
const std::string typeA("uncor,typeA");
for (auto& b : e1.bins()) {
  const double v = b.index()+1;
  b.setVal(v); b.setErr(0.1*v, typeA);
}
const auto covM = e1.covarianceMatrix();
for (size_t i = 0; i < covM.size(); ++i) {
  for (size_t j = 0; j < covM[i].size(); ++j) {
    std::cout << "\t(" << i << "," << j << ")=";
    std::cout << covM[i][j];
  }
  std::cout << std::endl;
}
std::cout << std::endl;
```
```
	(0,0)=0.04	(0,1)=0.06	(0,2)=0.08	(0,3)=0.1	(0,4)=0.12
	(1,0)=0.06	(1,1)=0.09	(1,2)=0.12	(1,3)=0.15	(1,4)=0.18
	(2,0)=0.08	(2,1)=0.12	(2,2)=0.16	(2,3)=0.2	(2,4)=0.24
	(3,0)=0.1	(3,1)=0.15	(3,2)=0.2	(3,3)=0.25	(3,4)=0.3
	(4,0)=0.12	(4,1)=0.18	(4,2)=0.24	(4,3)=0.3	(4,4)=0.36
```

## Type reductions

Assuming the at least two binning dimensions,
the `BinnedEstimate` object can be slices along one of the axes:

```cpp
Estimate2D e2(5, 0, 1, 5, 0, 1);
std::vector<Estimate1D> e1Ds = e2.mkEstimates<1>(); // along the second axis
```

It's also possible to create a `Scatter` object directly from
histograms and profiles:

```cpp
Estimate1D e3(5, 0, 1);
Scatter2D s1 = e3.mkScatter();
```

