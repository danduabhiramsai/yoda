# MPI (de-)serialization

The YODA analysis objects provide methods for data serialisation
and deserialisation for MPI communication, e.g. in rank reduce
operations.

An example could look like this

```python
from mpi4py import MPI
import yoda

def processRank(rank):
  yoda.Histo1D h(5, 0.0, 1.0, "H%i" % rank)
  # ... fill some events ...
  data = h.serializeContent()
  return data

mpi_comm = MPI.COMM_WORLD
mpi_rank = mpi_comm.Get_rank()
mpi_size = mpi_comm.Get_size()

res = processRank(mpi_rank)
res = mpi_comm.gather(res)

if mpi_rank == 0:
  h = yoda.Histo1D(5, 0.0, 1.0, "final")
  h.deserializeContent(res[0])
  htemp = h.clone()
  for data in res[1:]:
    htemp.deserializeContent(data)
    h += htemp
```

A C++ example could look like this:

```cpp
#include "YODA/Histo.h"
#include "YODA/WriterYODA.h"
#include <mpi.h>
#include <string>
#include <vector>

using namespace YODA;

int main() {

  MPI_Init(NULL, NULL);

  int process_id;
  MPI_Comm_rank(MPI_COMM_WORLD, &process_id);

  int num_proc;
  MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

  Histo1D h(5, 0.0, 1.0, "H" + std::to_string(process_id));
  // ... fill some events ...

  std::vector<double> content = h.serializeContent(true); //< ensure fixed length
  MPI_Allreduce(MPI_IN_PLACE, content.data(), content.size(),
                MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  if (process_id == 0) {
    h.deserializeContent(content);
    WriterYODA::write(std::cout, h);
  }

  MPI_Finalize();

  return 0;
}
```

Note that the `AnalysisObject::serializeContent()` method takes an optional
Boolean which, if set to true, will ensure that the resulting data vectors
will have a fixed length on each rank. It achieves this by skipping the content
of all Scatter objects as well as the uncertainty breakdown, if applicable.

