# The Binning object

YODA's binning class is templated on the Axis types.

An example using the `Axis` objects defined [here](BinnedAxis.md):

```cpp
using MyBinningT = Binning<Axis<double>, Axis<int>, Axis<std::string>>;
MyBinningT binning(caxis, daxis1, daxis2);
std::cout << "Binning object has " << binning.dim();
std::cout << " dimensions and " << binning.numBins(true);
std::cout << " number of bins in total" << std::endl;
```

```
Binning object has 3 dimensions and 128 number of bins in total
```

There are helper functions to translate the local indices from
an individual axis to the global index of the binning object,
either via index or via coordinate:

```cpp
std::cout << "Global index given local indices (1, 5, 2) is ";
std::cout << binning.localToGlobalIndex({1,5,2}) << std::endl;
std::cout << "Global index given coordinates (0.5, 1, B) is ";
std::cout << binning.globalIndexAt({0.5,1,"B"}) << std::endl;
```

```
Global index given local indices (1, 5, 2) is 85
Global index given coordinates (0.5, 1, B) is 85
```

