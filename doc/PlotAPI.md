# Plotting API

YODA offers direct plot functionality via the `yodaplot` script, but more flexibility is achieved by interacting with the Python-based plotting API directly. This page provides some guidance on how to set this up.

To get started, we provide a minimal example in Python to get some plots through the YODA API. Then we expand on this example to demonstrate the flexibility of the module, by tweaking graphical features and optimising the plot generation.

## Getting started with a minimal example
Plotting YODA files through the API works in two steps:

1. First, the plotting API generates a set of executable Python scripts (`.py`). These scripts contain `matplotlib` syntax to draw graphical outputs from the numerical inputs in the YODA file.
2. Then as a second step, the Python scripts need to be executed to actually make the plots and store them in your filesystem (e.g. in `.pdf` or `.png` format).


### Script generation
The main functionality responsible for generating the executable Python scripts comes from the `yoda.plotting` module, so let's import the relevant functionality from it.

```python
from yoda.plotting import script_generator
```

Then, we load our YODA file into memory and generate a Python script for each analysis object in that file:

```python
import yoda
fname = 'test.yoda.gz'
executable_python_scripts = []
for aopath, ao in yoda.read(fname).items():
    plotContent = { 'histograms' : { fname : { 'nominal' : ao } } }
    executable_python_scripts.append(script_generator.process(plotContent, aopath.split('/')[-1]))

```

The function `script_generator()` returns the path to the executable Python script and requires two arguments.

1. The first one is a nested dictionary containing the YODA analysis objects to be plotted and plot features
   (`plotContent` in the above example). The `'histograms'` dictionary is a mandatory entry for the plotting to work.
   Its contents are dictionaries themselves: one for each analysis object that one wants to plot.
   The keys are strings to keep track of separate input files, in this example `'test.yoda.gz'`.
   When operating on multiple YODA files, `'histograms'` requires an entry for each file.
   The `'nominals'` entry is mandatory and links to the analysis object.
2. A string indicating the name of the plot, which is propagated to the filenames of the Python script and the graphical outputs.

### Script execution

The plotting scripts can be executed from the command-line directly, but this can also be automated
in a script or function like `yodaplot` does. The scripts are self-contained and depend only on the
`matplotlib` and Numpy libraries. As there is no dependence on YODA in the plotting scripts,
they can be easily exchanged between different people and across different platforms, or even kept
in version control to (re)generate the plots at a later time.

## Extra features

### Tweaking the plotting scripts

By default, the API directs the outputs to the current working directory (`'.'`) and commands `matplotlib`
to store the files only in PDF format. The `script_generator()` function accepts two optional arguments,
`outdir` and `formats` respectively, to change these settings:

```python
script_generator.process(plotContent, 'analysis_object_1', outdir='/path/to/output/directory', formats=["PDF", "PNG"])
```

`outdir` should be a string pointing to the desired output directory, where `formats` should be a list
of strings with different graphical formats.

---

The `plotContent` dictionary can be expanded to modify graphical features of the plots themselves.
In the entries of the `histograms` dictionary, optional features include:

- `Title` (string) to change a YODA file's entry in the legend.
- `ErrorBars` (bool) to switch error bars on/off for the corresponding YODA file.
- `IsRef` (bool) to force a YODA file to be relative file to which ratios are calculated in the ratio panel.

---

Then, `plotContent` allows an additional entry `'plot features'` with more graphical options. These include:

- `Title` (string) to draw a title on the canvas.
- `XLabel`, `YLabel` (string) to set the labels of the X and Y axes respectively.
- `LogY` (bool) to force the Y-axis to be logarithmic
- `RatioPlot` (bool) to enable/disable the ratio panel.

More customisations can be implemented here or post-hoc by directly editing the output Python plotting scripts.

---

Furthermore, the `'style'` entry can be set to use a custom `.mplstyle` file in the API directly.
If no custom file is specified, it defaults to the YODA in-house plotting style as defined in `default.mpstyle` file.
The style can still be set after running the plotting API by tweaking the stylefile in the executable Python scripts.

---

Let's have a look at a practical example piecing all the above information together:

```python
plotContent = {
  'histograms' : {
    fname  : {
      'nominal' : ao,
      'Title' : 'File 1 Data',
      'ErrorBars' : True,
      'IsRef' : False,
    },
  }

  'plot features' : {
    'Title'     : 'API fun',
    'XLabel'    : '$x$',
    'YLabel'    : '$y$',
    'LogY'      : True,
    'RatioPlot' : True
  },

  'style' : 'CustomStyle',
}
```

For any given histogram, multiweight variation AOs can be supplied in the inner dictionary
with key that is prefixed with `multiweight`. If weights are combined into an uncertainty
band, the corresponding AO with inflated uncertainties can be supplied similarly using
the key `BandUncertainty`.
