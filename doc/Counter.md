# Counters

YODA's `Counter` class is like a histogram without
binned axes. It lends itself nicely to computations
of integrated event yields and such.

```cpp
Counter c;
c.fill();  c.fill(10);
std::cout << "# entries:      " << c.numEntries() << std::endl;
std::cout << "Sum of weights: " << c.sumW() << std::endl;
```
```
# entries:      2
Sum of weights: 11
```

It is possible to get an [Estimate object](Estimate.md) from
a `Counter` like so:

```cpp
Estimate e = c.mkEstimate();
```

