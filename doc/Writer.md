# Writers

YODA provides a few specialised writers
depending on the desired output formats.

## WriterYODA

The `WriterYODA` specialisation can be used
to write analysis objects in ASCII format,
e.g. here to `cout`:

```cpp
Counter c("C");
c.fill(10);
Histo1D h1(2,0,1, "H1D_d");
h1.fill(0.1, 10);
h1.fill(0.7,  7);
BinnedHisto1D<std::string> h2({"A"}, "H1D_s");
h2.fill(std::string("A"), 3);
h2.fill(std::string("not A"), 5);
YODA::WriterYODA::write(std::cout, c);
YODA::WriterYODA::write(std::cout, h1);
YODA::WriterYODA::write(std::cout, h2);
```
```
BEGIN YODA_COUNTER_V3 /C
Path: /C
Title: 
Type: Counter
---
# sumW       	sumW2        	numEntries
1.000000e+01 	1.000000e+02 	1.000000e+00 
END YODA_COUNTER_V3

BEGIN YODA_HISTO1D_V3 /H1D_d
Path: /H1D_d
Title: 
Type: Histo1D
---
# Mean: 3.470588e-01
# Integral: 1.700000e+01
Edges(A1): [0.000000e+00, 5.000000e-01, 1.000000e+00]
# sumW       	sumW2        	sumW(A1)     	sumW2(A1)    	numEntries
0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00 
1.000000e+01 	1.000000e+02 	1.000000e+00 	1.000000e-01 	1.000000e+00 
7.000000e+00 	4.900000e+01 	4.900000e+00 	3.430000e+00 	1.000000e+00 
0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00 
END YODA_HISTO1D_V3

BEGIN YODA_BINNEDHISTO<S>_V3 /H1D_s
Path: /H1D_s
Title: 
Type: BinnedHisto<s>
---
# Mean: 3.750000e-01
# Integral: 8.000000e+00
Edges(A1): ["A"]
# sumW       	sumW2        	sumW(A1)     	sumW2(A1)    	numEntries
5.000000e+00 	2.500000e+01 	0.000000e+00 	0.000000e+00 	1.000000e+00 
3.000000e+00 	9.000000e+00 	3.000000e+00 	3.000000e+00 	1.000000e+00 
END YODA_BINNEDHISTO<S>_V3
```

`WriterYODA` can also be used to write analysis objects
to disk, e.g. as an ASCII file, like so:

```cpp
std::vector<AnalysisObject*> aos = { &c, &h1, &h2 };
YODA::Writer& aoWriter = YODA::WriterYODA::create();
aoWriter.write("output.yoda", aos);
```

## WriterFLAT

The `WriterFLAT` specialisation will convert all analysis objects
to an appropriate `Scatter` object first before writing out the
scatters:

```cpp
YODA::WriterFLAT::write(std::cout, c);
YODA::WriterFLAT::write(std::cout, h1);
YODA::WriterFLAT::write(std::cout, h2);
```
```
BEGIN SCATTER1D /C
Path=/C
Title=
# val1       	err1-        	err1+        	
1.000000e+01 	1.000000e+01 	1.000000e+01 	
END SCATTER1D

BEGIN SCATTER2D /H1D_d
Path=/H1D_d
Title=
# val1       	err1-        	err1+        	val2         	err2-        	err2+        	
2.500000e-01 	2.500000e-01 	2.500000e-01 	2.000000e+01 	2.000000e+01 	2.000000e+01 	
7.500000e-01 	2.500000e-01 	2.500000e-01 	1.400000e+01 	1.400000e+01 	1.400000e+01 	
END SCATTER2D

BEGIN SCATTER2D /H1D_s
Path=/H1D_s
Title=
# val1       	err1-        	err1+        	val2         	err2-        	err2+        	
1.000000e+00 	0.000000e+00 	0.000000e+00 	3.000000e+00 	3.000000e+00 	3.000000e+00 	
END SCATTER2D
```

## WriterH5

In development ...

