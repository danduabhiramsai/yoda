import re

## Simple replacement map
_macros = {
  r'\GeV'   : r'\mathrm{Ge\!V}',
  r'\TeV'   : r'\mathrm{Te\!V}',
  r'\pt'    : r'{p_\mathrm{T}}',
  r'\pT'    : r'{p_\mathrm{T}}',
  r'\dfrac' : r'\frac',
  r'\tfrac' : r'\frac',
}

## Replacement map that triggers on a regex
_regex = {
  r"\$(.+?textrm.+?)\$" : [ r"\textrm", r"\mathrm" ],
  r"\$(.+?text.+?)\$" : [ r"\text", r"\mathrm" ],
  r"\$(.+?'.+?)\$" : [ r"'", r"\prime" ],
}

def preprocess(s):
    """Apply simple and regex-based
       substitutions defined in this file."""
    if not isinstance(s, str):
        return s

    for _from, _to in _macros.items():
        s = s.replace(_from, _to)

    for regex, (_from, _to) in _regex.items():
        if re.search(regex, s):
            s = s.replace(_from, _to)

    return s.replace(r'\newline', r'"+"\n"+r"')

