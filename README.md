# YODA - Yet more Objects for Data Analysis

YODA is a small set of data analysis (specifically histogramming) classes being developed by
[MCnet](https://www.montecarlonet.org/) members as a lightweight common system for MC event generator
validation analyses, particularly as the core histogramming system in [Rivet](http://rivet.hepforge.org/).

YODA is a refreshingly clean, natural and powerful way to do histogramming...
and there are plenty of improvements still to come. Our mission is to make the most powerful,
expressive, and focused approach to binned computational data handling, with the nicest possible
balance of power and simplicity in the user interface. We hope you'll agree it's a good thing,
but if not (or even if so) please get in touch and let us know about your thoughts, problems,
and feature requests.

## Getting started

[Installation](doc/installation.md)

## Tell me more about ...

**Frontend classes:**

[Dbn: the distribution class](doc/Dbn.md)

[Estimate: central values and uncertainty breakdowns](doc/Estimate.md)

[Point: coordinates with error bars](doc/Point.md)

[AnalysisObject](doc/AnalysisObject.md)

[Counters](doc/Counter.md)

[Estimate0D](doc/Estimate0D.md)

[BinnedDbn, Histograms and Profiles](doc/BinnedDbn.md)

[BinnedEstimate (inert datasets)](doc/BinnedEstimate.md)

[Scatters (arrays of Points)](doc/Scatter.md)


**I/O:**

[YODA format layout](doc/YodaFormat.md)

[Writers](doc/Writer.md)

[Readers](doc/Reader.md)

[MPI (de-)serialization](doc/MPI.md)


**Backend classes:**

[Axis class: continuous and discrete axes](doc/BinnedAxis.md)

[Binning class: the Axis container](doc/Binning.md)

[Bin class: the storage unit](doc/Bin.md)

[BinnedStorage and derived storage classes](doc/BinnedStorage.md)

**Plotting API:**

[Plotting API](doc/PlotAPI.md)

[Plotting style customisation](doc/PlotConfig.md)

