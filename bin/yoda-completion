## -*- sh -*-
## Analysis name completion for YODA scripts

## Load bash completion compatibility if zsh is used
if test -n "$ZSH_VERSION"; then
    autoload bashcompinit
    bashcompinit
fi


## Create a _filedir helper function if not already present
(type _filedir &> /dev/null) || \
function _filedir() {
    local cur prev commands options command
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(compgen -W "$(ls ${cur}* 2> /dev/null)" -- ${cur}) )
    return 0
}


##########################


function _yoda_config() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help --version"
    opts="$opts --prefix --includedir --libdir --pythonpath"
    opts="$opts --cxxflags --libs"
    opts="$opts -h"
    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    COMPREPLY=( $(compgen -W "$opts" -- ${cur}) )
    if test -n "$COMPREPLY"; then
        return 0
    fi

    return 0
}


complete -F _yoda_config yoda-config


##############################


function _yodals() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --quiet -q"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodals -o default yodals


##############################


function _yodadiff() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --output -o --tol -t"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodadiff -o default yodadiff


##############################


function _yodamerge() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --output -o --match -m --unmatch -M -v --verbose -q --quiet"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodamerge -o default yodamerge


##############################


function _yodastack() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --output -o --match -m --unmatch -M -v --verbose -q --quiet"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodastack -o default yodastack


##############################


function _yodascale() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --refs -r --ref-prefix"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodascale -o default yodascale


##############################


function _yodaplot() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --format -f --jobs -j --debug --quiet"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    elif [[ ${prev} =~ "--format|-f" ]] ; then
        COMPREPLY=( $(compgen -W "PDF PNG TEX PGF" -- ${cur}) )
    fi
    if test -n "$COMPREPLY"; then return 0; fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodaplot -o default yodaplot


##############################


function _yodaenvelope() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --refs -r"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodaenvelope -o default yodaenvelope


##############################


function _yodacnv() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --match -m --unmatch -M"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yodacnv -o default yodacnv


#############################


function _yoda2root() {
    local cur prev commands options command
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts="--help -h --match -m --unmatch -M"

    if [[ ${cur} = -* ]] ; then
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        if test -n "$COMPREPLY"; then return 0; fi
    fi

    _filedir "yoda|yoda.gz"
    return 0
}


complete -F _yoda2root -o default yoda2root


