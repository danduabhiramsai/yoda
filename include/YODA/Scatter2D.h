// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_Scatter2D_h
#define YODA_Scatter2D_h

#include "YODA/Scatter.h"
#pragma message "Scatter2D.h is deprecated. Please use Scatter.h instead."

#endif
