// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_POINT1D_H
#define YODA_POINT1D_H

#include "YODA/Point.h"
#pragma message "Point1D.h is deprecated. Please use Point.h instead."

#endif
