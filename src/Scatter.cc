#include "YODA/Scatter.h"
#include "YODA/Counter.h"

namespace YODA {


  /// Make a Scatter1D representation of a Histo1D
  Scatter1D mkScatter(const Counter& c) {
    Scatter1D rtn;
    for (const std::string& a : c.annotations())
      rtn.setAnnotation(a, c.annotation(a));
    rtn.setAnnotation("Type", c.type()); // might override the copied ones
    Point1D pt(c.val(), c.err());
    rtn.addPoint(pt);
    return rtn;
  }

}
