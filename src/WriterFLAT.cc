// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#include "YODA/WriterFLAT.h"
#include "YODA/Scatter.h"

#include <iostream>
#include <iomanip>

using namespace std;

namespace YODA {

  /// Singleton creation function
  Writer& WriterFLAT::create() {
    static WriterFLAT _instance;
    _instance.setPrecision(6);
    return _instance;
  }

  // Version-formatting helper function
  void WriterFLAT::_writeAnnotations(std::ostream& os, const AnalysisObject& ao) {
    os << scientific << setprecision(_aoprecision);
    for (const string& a : ao.annotations()) {
      if (a.empty()) continue;
      if (a == "Type") continue;
      /// @todo Should write out floating point annotations as scientific notation...
      os << a << "=" << ao.annotation(a) << "\n";
    }
  }


  void WriterFLAT::writeAO(std::ostream& os, const AnalysisObject& ao) {
    ios_base::fmtflags oldflags = os.flags();
    os << scientific << showpoint << setprecision(_aoprecision);
    os << "BEGIN SCATTER" <<  std::to_string(ao.dim()) << "D " << ao.path() << "\n";
    _writeAnnotations(os, ao);
    ao._renderFLAT(os, _aoprecision+7); // = "-1." + _aoprecision + "e+23"
    os << "END SCATTER" << std::to_string(ao.dim()) << "D" << "\n\n";

    os << flush;
    os.flags(oldflags);
  }

}
